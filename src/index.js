#!/usr/bin/env node
const fetch = require("cross-fetch");
const fs = require("fs");
const { loadConfig } = require("graphql-config");
const envConfig = require("dotenv").config({
  path: "./.env.local",
});

const configDefaults = {
  gitlabToken: process.env["GITLAB_TOKEN"] || envConfig.GITLAB_TOKEN,
  gitlabUrl: process.env["GL_URL"] || "https://gitlab.com",
  gitlabPrefix: process.env["GL_PREFIX"] || "/api/v4",
  jobName: "Schema",
  operationsUrl: "operations.json",
  schemaUrl: "schema.json",
  localFolder: "./.schema-registry",
};

console.log(configDefaults);

function ensureDir(dir) {
  if (!fs.existsSync(dir)) {
    return fs.mkdirSync(dir);
  }
}

const writeJSONToFile = (object, path = "schema.json") => {
  return fs.writeFileSync(path, JSON.stringify(object, null, 2));
};

async function loadRegistryConfig() {
  const graphqlConfig = await loadConfig({ throwOnEmpty: true });
  const projectConfig = graphqlConfig.getDefault();
  const registryConfig = projectConfig.extensions.registry;

  return {
    ...configDefaults,
    ...registryConfig,
  };
}

const getGitlabArtifactUrl = ({
  ref,
  projectUrl,
  gitlabUrl,
  gitlabPrefix,
  jobName,
  artifactUrl,
}) => {
  return `${gitlabUrl}${gitlabPrefix}/projects/${encodeURIComponent(
    projectUrl
  )}/jobs/artifacts/${ref}/raw/${artifactUrl}?job=${jobName}`;
};

const writeToFile = (object, name = "schema.json") => {
  return fs.writeFileSync(name, JSON.stringify(object, null, 2));
};

async function getArtifact({
  ref,
  projectUrl,
  gitlabUrl,
  gitlabPrefix,
  jobName,
  artifactUrl,
  gitlabToken,
}) {
  const url = getGitlabArtifactUrl({
    ref,
    projectUrl,
    gitlabUrl,
    gitlabPrefix,
    jobName,
    artifactUrl,
  });

  // eslint-disable-next-line
  console.log(`
    fetching artifact
      ref: ${ref}
      artifact: ${artifactUrl}
      project: ${projectUrl}
      gitlabURL: ${url}
  `);

  const response = await fetch(url, {
    headers: {
      "PRIVATE-TOKEN": gitlabToken,
    },
  });

  const data = await response.json();
  if (!data || data.error || data.message) {
    console.error(data);
    throw Error("failed to get artifact");
  }
  return data;
}

async function getSchema({ ref = "master", jobName = "Schema" }) {
  const config = await loadRegistryConfig();
  const name = config.schemaUrl;
  const url = getGitlabArtifactUrl({
    ref: "master",
    projectUrl: config.server,
    gitlabUrl: config.gitlabUrl,
    gitlabPrefix: config.gitlabPrefix,
    jobName,
    artifactUrl: config.schemaUrl,
  });
  // eslint-disable-next-line
  console.log(`
    fetching schema
      ref: ${ref}
      name: ${config.schemaUrl}
      project: ${config.server}
      url: ${url}
  `);

  const response = await fetch(url, {
    headers: {
      "PRIVATE-TOKEN": config.gitlabToken,
    },
  });

  const data = await response.json();
  if (!data.__schema) {
    console.error(data);
    throw Error("failed to get schema");
  }

  writeToFile(data, name);

  // eslint-disable-next-line
  console.log(`
    Successfully fetched schema 
  `);
  return data;
}

async function getClientOperations(project, ref, config) {
  const projectName = project.split("/")[1];
  const operations = await getArtifact({
    ref,
    projectUrl: project,
    gitlabUrl: config.gitlabUrl,
    gitlabPrefix: config.gitlabPrefix,
    jobName: config.jobName,
    artifactUrl: config.operationsUrl,
    gitlabToken: config.gitlabToken,
  });

  operations.forEach((operation, index) => {
    fs.writeFileSync(
      `${config.localFolder}/${projectName}-${index + 1}.graphql`,
      operation.rawSDL.normalize()
    );
  });

  writeJSONToFile(operations, `${config.localFolder}/${projectName}.json`);
  return operations;
}

async function getOperations({ ref = "master" }) {
  const config = await loadRegistryConfig();
  const { localFolder, clients = [] } = config;

  if (clients.length === 0) {
    console.log(`
    ***
      No clients specified

      Skipping... 
      
      specify clients in graphql.config.yml
        registry:
          clients:
            - '{gitlab project url}
    *** `);
    return [];
  }
  ensureDir(localFolder);
  const operationsArtifacts = await Promise.all(
    clients.map((client) => getClientOperations(client, ref, config))
  );
  return operationsArtifacts;
}

require("yargs")
  .usage("$0 <cmd> [args]")
  .command(
    "fetch-schema [ref]",
    "fetches schema from artifactary at the target branch",
    () => {},
    getSchema
  )
  .command(
    "fetch-operations [ref]",
    "fetches and writes the operations from the target",
    () => {},
    getOperations
  )
  .help()
  .showHelpOnFail(true)
  .demandCommand(1, "")
  .parse();
